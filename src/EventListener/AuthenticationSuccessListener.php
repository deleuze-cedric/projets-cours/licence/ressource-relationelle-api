<?php

namespace App\EventListener;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessListener
{

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $userData = new \stdClass();

        if ($user instanceof User) {

            $activatedDate = $user->getActivatedDate();

            $userData->uuid = $user->getUuid();
            $userData->nom = $user->getNom();
            $userData->prenom = $user->getPrenom();
            $userData->pseudo = $user->getPseudo();
            $userData->email = $user->getEmail();
            $userData->roles = $user->getRoles();
            $userData->image = $user->getImage();
            $userData->activatedDate = empty($activatedDate) ? null : $activatedDate->format('Y-m-d H:i:s');
            $userData->deletedDate = empty($user->getDeletedDate()) ? null : $user->getDeletedDate()->format('Y-m-d H:i:s');
            $userData->updatedDate = empty($user->getUpdatedDate()) ? null : $user->getUpdatedDate()->format('Y-m-d H:i:s');
            $userData->createdDate = empty($user->getCreatedDate()) ? null : $user->getCreatedDate()->format('Y-m-d H:i:s');
            $userData->activationRequired = empty($activatedDate);

        }

        $data['user'] = $userData;

        $event->setData($data);
    }

}
