<?php

namespace App\Security;

use App\Entity\User;
use App\Functions\Functions;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $user = !empty($this->security->getToken()) ? $this->security->getToken()->getUser() : null;
        $content = "Vous devez être connecté pour accéder à cette page";
        $activatedRoles = Functions::getRolesBOList();
        $isRoleAdmin = false;

        if (!empty($user) && ($user instanceof User || $user instanceof \App\Document\User)) {
            foreach ($user->getRoles() as $role) {
                if (in_array($role, $activatedRoles)) {
                    $isRoleAdmin = true;
                    break;
                }
            }
            if (!$isRoleAdmin && empty($user->getActivatedDate())) {
                $content = "Votre compte n'est pas actif";
            } else {
                $content = "Vous n'êtes pas autorisé à accéder à ces données";
            }

        }

        $resp = new \stdClass();
        $resp->code = 403;
        $resp->message = $content;
        return new JsonResponse($resp, 403);
    }
}