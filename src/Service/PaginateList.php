<?php


namespace App\Service;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class PaginateList
{

    /**
     * @param ServiceEntityRepository $repo
     * @param Request $request
     * @param int $id
     * @param string $functionRepo
     * @return Paginator
     * @throws Exception
     */
    public function getPaginateListFromRepo(ServiceEntityRepository $repo, Request $request, int $id, string $functionRepo): Paginator
    {
        $page = (int) $request->query->get('page', 1);
        $itemsPerPage = (int) $request->query->get('itemsPerPage', 20);

        if(method_exists($repo, $functionRepo)){
            return $repo->$functionRepo($id, $page, $itemsPerPage);
        } else {
            throw new Exception('Function not found');
        }
    }
}