<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeRelationRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TypeRelationRepository::class)
 * @ApiResource(
 *     itemOperations={
 *          "get",
 *          "put"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *          "patch"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"={"security_post_denormalize"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *     normalizationContext={
 *          "groups"={"type_relation.read","type_relation.all"}
 *     },
 *     denormalizationContext={
 *          "groups"={"type_relation.write", "type_relation.all"}
 *     },
 *     paginationEnabled=false
 * )
 */
class TypeRelation
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=false)
     */
    protected $id;

    /**
     * @var String
     * @ORM\Column(type="string", unique=true)
     * @ApiProperty(identifier=true)
     * @Gedmo\Slug(fields={"libelle"})
     * @Groups({
     *     "type_relation.read",
     *     "relation.read",
     *     "user.read",
     * })
     */
    protected $slug;

    /**
     * @var String
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez renseigner le libelle")
     * @Gedmo\Language()
     * @Groups({
     *     "type_relation.all",
     *     "relation.read",
     *     "user.read",
     * })
     */
    protected $libelle;

    /**
     * @Gedmo\Locale
     */
    protected $locale;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="typeRelation", orphanRemoval=true)
//     * @ApiProperty(readableLink=false, writableLink=false)
//     * @Groups({
//     *     "type_relation.read"
//     * })
//     */
//    protected $lsRelations;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "type_relation.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "type_relation.read"
     * })
     */
    protected $updatedDate;

//
//    /**
//     * @var DateTime
//     * @ORM\Column(type="datetime", nullable=true)
//     * @Groups({
//     *     "type_relation.read"
//     * })
//     */
//    protected $deletedDate;
//
//    public function __construct()
//    {
//        $this->lsRelations = new ArrayCollection();
//    }

    /**
     * @return int id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }
//
//    public function getDeletedDate(): ?DateTime
//    {
//        return $this->deletedDate;
//    }
//
//    public function delete(bool $deleted): self
//    {
//        if ($deleted) {
//            $this->deletedDate = new DateTime();
//        } else {
//            $this->deletedDate = new DateTime();
//        }
//    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getLibelle()
    {
        return $this->libelle;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
//
//    /**
//     * @return Collection|Relation[]
//     */
//    public function getLsRelations(): Collection
//    {
//        return $this->lsRelations;
//    }
//
//    public function addLsRelation(Relation $lsRelation): self
//    {
//        if (!$this->lsRelations->contains($lsRelation)) {
//            $this->lsRelations[] = $lsRelation;
//            $lsRelation->setTypeRelation($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsRelation(Relation $lsRelation): self
//    {
//        if ($this->lsRelations->removeElement($lsRelation)) {
//            // set the owning side to null (unless already changed)
//            if ($lsRelation->getTypeRelation() === $this) {
//                $lsRelation->setTypeRelation(null);
//            }
//        }
//
//        return $this;
//    }

}
