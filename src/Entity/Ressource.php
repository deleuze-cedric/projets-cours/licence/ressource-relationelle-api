<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\RessourceFilter;
use App\Repository\RessourceRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Filter\CustomUserFilter;
use App\Filter\CustomTypeRelationFilter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RessourceRepository", repositoryClass=RessourceRepository::class)
 * @ApiResource(
 *     itemOperations={
 *      "get",
 *      "put"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getCreateur() == user)"},
 *      "patch"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getCreateur() == user)"},
 *      "delete"={"security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.getCreateur() == user)"}
 *     },
 *     collectionOperations={
 *      "get",
 *      "post"
 *     },
 *     normalizationContext={"groups"={"ressource.read", "ressource.all"}},
 *     denormalizationContext={"groups"={"ressource.write", "ressource.all"}},
 *      attributes={
 *     "pagination_client_items_per_page"=true
 *     }
 * )
 * @ApiFilter(SearchFilter::class,
 *  properties={
 *     "titre": "partial",
 *     "categorieRessource.slug": "exact",
 *     "typeRessource.slug": "exact",
 *     "createur.pseudo": "partial",
 *     "createur.nom": "partial",
 *     "createur.prenom": "partial",
 *  }
 * )
 * @ApiFilter(DateFilter::class,
 *     properties={
 *     "createdDate",
 *     "updatedDate",
 *     "deletedDate"
 *     }
 * )
 * @ApiFilter(ExistsFilter::class, properties={"deletedDate"})
 * @ApiFilter(OrderFilter::class, properties={
 *     "updatedDate": "DESC",
 * })
 * @ApiFilter(CustomUserFilter::class,
 *  properties={
 *     "custom_user_filter"
 *   }
 * )
 * @ApiFilter(CustomTypeRelationFilter::class,
 *  properties={
 *     "custom_type_relation_filter"
 *   }
 * )
 */
class Ressource implements CreateByUser
{

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ApiProperty(identifier=false)
     */
    protected $id;

    /**
     * @var String
     * @ORM\Column(type="string", unique=true)
     * @ApiProperty(identifier=true)
     * @Gedmo\Slug(fields={"titre"})
     * @Groups({
     *     "ressource.read",
     *     "user.read"
     * })
     */
    protected $slug;

    /**
     * @var String
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Veuillez renseigner le titre")
     * @Assert\Length(max=255, maxMessage="Veuillez renseigner un titre avec moins de 255 caractères")
     * @Gedmo\Language
     * @Groups({
     *     "ressource.all",
     *     "user.read"
     * })
     */
    protected $titre;

    /**
     * @ORM\Column(type="json")
     * @Gedmo\Language
     * @Assert\NotBlank(message="Veuillez renseigner un contenu")
     * @Groups({
     *     "ressource.all",
     *     "user.read"
     * })
     */
    protected $contenu = [];

//, inversedBy="lsRessources"
    /**
     * @ORM\ManyToOne(targetEntity=CategorieRessource::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Veuillez renseigner une catégorie")
     * @Groups({
     *     "ressource.all",
     *     "user.read"
     * })
     */
    protected $categorieRessource;

//, inversedBy="lsRessources"
    /**
     * @ORM\ManyToOne(targetEntity=TypeRessource::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Veuillez renseigner un type de ressource")
     * @Groups({
     *     "ressource.all",
     *     "user.read"
     * })
     */
    protected $typeRessource;

//    , inversedBy="lsRessources"
    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Veuillez renseigner un créateur")
     * @Groups({
     *     "ressource.all",
     *     "user.read"
     * })
     */
    protected $createur;

    /**
     * @Gedmo\Locale
     */
    protected $locale;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="ecritPour", orphanRemoval=true)
//     * @ApiProperty(writableLink=false, readableLink=false)
//     */
//    protected $lsCommentaires;
//
//    /**
//     * @ORM\OneToMany(targetEntity=EtatRessourceUtilisateur::class, mappedBy="ressource", orphanRemoval=true)
//     * @ApiProperty(writableLink=false, readableLink=false)
//     */
//    protected $etatRessourceUtilisateurs;

//    /**
//     * @ORM\OneToMany(targetEntity=Partage::class, mappedBy="ressourcePartage", orphanRemoval=true)
//     * @ApiProperty(writableLink=false, readableLink=false)
//     */
//    protected $lsPartages;
//
//    /**
//     * @ORM\OneToMany(targetEntity=Discussion::class, mappedBy="ressourceSupport")
//     * @ApiProperty(writableLink=false, readableLink=false)
//     */
//    protected $lsDiscussions;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Groups({
     *     "ressource.read",
     *     "user.read"
     * })
     */
    protected $createdDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({
     *     "ressource.read",
     *     "user.read"
     * })
     */
    protected $updatedDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({
     *     "ressource.read",
     *     "user.read"
     * })
     */
    protected $deletedDate;

    /**
     * @ORM\ManyToOne(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     * @Groups({
     *     "ressource.all",
     *     "user.read"
     * })
     */
    private $image;

    public function __construct()
    {
//        $this->lsCommentaires = new ArrayCollection();
//        $this->etatRessourceUtilisateurs = new ArrayCollection();
        $this->lsPartages = new ArrayCollection();
//        $this->lsDiscussions =1 new ArrayCollection();
    }

    public function getContenu(): ?array
    {
        return $this->contenu;
    }

    public function setContenu(array $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getCategorieRessource(): ?CategorieRessource
    {
        return $this->categorieRessource;
    }

    /**
     * @Groups({
     *     "ressource.all"
     * })
     */
    public function setCategorieRessource(?CategorieRessource $categorieRessource): self
    {
        $this->categorieRessource = $categorieRessource;

        return $this;
    }

    public function getTypeRessource(): ?TypeRessource
    {
        return $this->typeRessource;
    }

    /**
     * @Groups({
     *     "ressource.all"
     * })
     * @param TypeRessource|null $typeRessource
     * @return Ressource
     */
    public function setTypeRessource(?TypeRessource $typeRessource): self
    {
        $this->typeRessource = $typeRessource;

        return $this;
    }

    public function getCreateur(): ?User
    {
        return $this->createur;
    }

    public function setCreateur(?User $createur): self
    {
        $this->createur = $createur;

        return $this;
    }

    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getUpdatedDate(): ?DateTime
    {
        return $this->updatedDate;
    }

    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }

    public function delete(bool $deleted): self
    {
        if ($deleted) {
            $this->deletedDate = new DateTime();
        } else {
            $this->deletedDate = new DateTime();
        }
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
//
//    /**
//     * @return Collection|Commentaire[]
//     */
//    public function getLsCommentaires(): Collection
//    {
//        return $this->lsCommentaires;
//    }
//
//    public function addLsCommentaire(Commentaire $lsCommentaire): self
//    {
//        if (!$this->lsCommentaires->contains($lsCommentaire)) {
//            $this->lsCommentaires[] = $lsCommentaire;
//            $lsCommentaire->setEcritPour($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsCommentaire(Commentaire $lsCommentaire): self
//    {
//        if ($this->lsCommentaires->removeElement($lsCommentaire)) {
//            // set the owning side to null (unless already changed)
//            if ($lsCommentaire->getEcritPour() === $this) {
//                $lsCommentaire->setEcritPour(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|EtatRessourceUtilisateur[]
//     */
//    public function getEtatRessourceUtilisateurs(): Collection
//    {
//        return $this->etatRessourceUtilisateurs;
//    }
//
//    public function addEtatRessourceUtilisateur(EtatRessourceUtilisateur $etatRessourceUtilisateur): self
//    {
//        if (!$this->etatRessourceUtilisateurs->contains($etatRessourceUtilisateur)) {
//            $this->etatRessourceUtilisateurs[] = $etatRessourceUtilisateur;
//            $etatRessourceUtilisateur->setRessource($this);
//        }
//
//        return $this;
//    }
//
//    public function removeEtatRessourceUtilisateur(EtatRessourceUtilisateur $etatRessourceUtilisateur): self
//    {
//        if ($this->etatRessourceUtilisateurs->removeElement($etatRessourceUtilisateur)) {
//            // set the owning side to null (unless already changed)
//            if ($etatRessourceUtilisateur->getRessource() === $this) {
//                $etatRessourceUtilisateur->setRessource(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Partage[]
//     */
//    public function getLsPartages(): Collection
//    {
//        return $this->lsPartages;
//    }
//
//    public function addLsPartage(Partage $lsPartage): self
//    {
//        if (!$this->lsPartages->contains($lsPartage)) {
//            $this->lsPartages[] = $lsPartage;
//            $lsPartage->setRessourcePartage($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsPartage(Partage $lsPartage): self
//    {
//        if ($this->lsPartages->removeElement($lsPartage)) {
//            // set the owning side to null (unless already changed)
//            if ($lsPartage->getRessourcePartage() === $this) {
//                $lsPartage->setRessourcePartage(null);
//            }
//        }
//
//        return $this;
//    }
//
//    /**
//     * @return Collection|Discussion[]
//     */
//    public function getLsDiscussions(): Collection
//    {
//        return $this->lsDiscussions;
//    }
//
//    public function addLsDiscussion(Discussion $lsDiscussion): self
//    {
//        if (!$this->lsDiscussions->contains($lsDiscussion)) {
//            $this->lsDiscussions[] = $lsDiscussion;
//            $lsDiscussion->addRessourceSupport($this);
//        }
//
//        return $this;
//    }
//
//    public function removeLsDiscussion(Discussion $lsDiscussion): self
//    {
//        if ($this->lsDiscussions->removeElement($lsDiscussion)) {
//            $lsDiscussion->removeRessourceSupport($this);
//        }
//
//        return $this;
//    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->getCreateur();
    }

    public function setUser(UserInterface $user)
    {
        return $this->setCreateur($user);
    }
}
