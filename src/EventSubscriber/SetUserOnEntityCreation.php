<?php
// api/src/EventSubscriber/ResolveMediaObjectContentUrlSubscriber.php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\CreateByUser;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use Vich\UploaderBundle\Storage\StorageInterface;

final class SetUserOnEntityCreation implements EventSubscriberInterface
{

    private Security $security;
    private EntityManager $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['setUserToResource', EventPriorities::PRE_WRITE],
        ];
    }

    /**
     * @param ViewEvent $event
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setUserToResource(ViewEvent $event): void
    {
        $object = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (
            !$object instanceof CreateByUser ||
            !in_array($method, [
                    Request::METHOD_POST,
                    Request::METHOD_PATCH,
                    Request::METHOD_PUT
                ]
            )
        ) {
            return;
        }

        $user = $this->security->getUser();

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $userAdmin = $object->getUser();
            if (!empty($userAdmin)) {
                $user = $userAdmin;
            }
        }

        $object->setUser($user);

        $this->entityManager->persist($object);
        $this->entityManager->flush();

    }
}