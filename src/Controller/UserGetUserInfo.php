<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class UserGetUserInfo extends PaginateListAction
{
    /**
     * @param User $data
     * @param UserRepository $repository
s     * @return void
     */
    public function __invoke(User $data, UserRepository $repository)
    {
        return $repository->getInfo($data);
    }
}