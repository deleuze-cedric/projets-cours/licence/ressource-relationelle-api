<?php


namespace App\Controller;

use ApiPlatform\Core\DataProvider\Pagination;
use App\Entity\CategorieRessource;
use App\Entity\User;
use App\Repository\CategorieRessourceRepository;
use App\Repository\UserRepository;
use App\Service\SendMail;
use App\Service\UserActiveAccount;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class UserActivateAccountAction
{
    private $activeAccount;

    public function __construct(UserActiveAccount $activeAccount)
    {
        $this->activeAccount = $activeAccount;
    }

    /**
     * @param String $token
     * @return JsonResponse
     */
    public function __invoke(String $token) : JsonResponse
    {
        return $this->activeAccount->activeAccount($token);
    }

}