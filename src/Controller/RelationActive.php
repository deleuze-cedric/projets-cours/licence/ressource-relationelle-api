<?php

namespace App\Controller;

use App\Entity\Relation;
use App\Entity\User;
use App\Repository\RelationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class RelationActive extends PaginateListAction
{
    /**
     * @param Relation $data
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Relation
     */
    public function __invoke(Relation $data, Request $request, EntityManagerInterface $em)
    {
        $json = $request->getContent();
        if (!empty("json")) {

            $jsonParse = json_decode($json);
            $relation = $data->accepte($jsonParse->active ?? false);
            $em->persist($data);
            $em->flush();

            return $relation;

        } else {
            return $data;
        }
    }
}