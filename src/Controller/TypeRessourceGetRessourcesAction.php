<?php


namespace App\Controller;

use ApiPlatform\Core\DataProvider\Pagination;
use App\Entity\CategorieRessource;
use App\Entity\TypeRessource;
use App\Repository\CategorieRessourceRepository;
use App\Repository\TypeRessourceRepository;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class TypeRessourceGetRessourcesAction extends PaginateListAction
{

    /**
     * @param TypeRessource $data
     * @param Request $request
     * @param TypeRessourceRepository $typeRessourceRepository
     * @return Paginator
     * @throws Exception
     */
    public function __invoke(TypeRessource $data, Request $request, TypeRessourceRepository $typeRessourceRepository): Paginator
    {
        return $this->paginator->getPaginateListFromRepo($typeRessourceRepository, $request, $data->getId(), 'getListeRessources');
    }
}