<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201112175856 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE type_relation ADD slug VARCHAR(255) NOT NULL, ADD titre VARCHAR(255) NOT NULL, ADD updated_date DATETIME DEFAULT NULL, ADD activated_date DATETIME DEFAULT NULL, ADD deleted_date DATETIME DEFAULT NULL, ADD created_date DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE type_relation DROP slug, DROP titre, DROP updated_date, DROP activated_date, DROP deleted_date, DROP created_date');
    }
}
