<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201205163223 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categorie_ressource DROP FOREIGN KEY FK_3EC04A43F98F144A');
        $this->addSql('DROP INDEX IDX_3EC04A43F98F144A ON categorie_ressource');
        $this->addSql('ALTER TABLE categorie_ressource DROP logo_id');
        $this->addSql('ALTER TABLE type_relation DROP FOREIGN KEY FK_AF7660B5F98F144A');
        $this->addSql('DROP INDEX IDX_AF7660B5F98F144A ON type_relation');
        $this->addSql('ALTER TABLE type_relation DROP logo_id');
        $this->addSql('ALTER TABLE type_ressource DROP FOREIGN KEY FK_27590454F98F144A');
        $this->addSql('DROP INDEX IDX_27590454F98F144A ON type_ressource');
        $this->addSql('ALTER TABLE type_ressource DROP logo_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categorie_ressource ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE categorie_ressource ADD CONSTRAINT FK_3EC04A43F98F144A FOREIGN KEY (logo_id) REFERENCES media_object (id)');
        $this->addSql('CREATE INDEX IDX_3EC04A43F98F144A ON categorie_ressource (logo_id)');
        $this->addSql('ALTER TABLE type_relation ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE type_relation ADD CONSTRAINT FK_AF7660B5F98F144A FOREIGN KEY (logo_id) REFERENCES media_object (id)');
        $this->addSql('CREATE INDEX IDX_AF7660B5F98F144A ON type_relation (logo_id)');
        $this->addSql('ALTER TABLE type_ressource ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE type_ressource ADD CONSTRAINT FK_27590454F98F144A FOREIGN KEY (logo_id) REFERENCES media_object (id)');
        $this->addSql('CREATE INDEX IDX_27590454F98F144A ON type_ressource (logo_id)');
    }
}
