<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201114083616 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE discussion_ressource');
        $this->addSql('ALTER TABLE discussion ADD ressource_support_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE discussion ADD CONSTRAINT FK_C0B9F90FCA7FDC09 FOREIGN KEY (ressource_support_id) REFERENCES ressource (id)');
        $this->addSql('CREATE INDEX IDX_C0B9F90FCA7FDC09 ON discussion (ressource_support_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE discussion_ressource (discussion_id INT NOT NULL, ressource_id INT NOT NULL, INDEX IDX_10F998121ADED311 (discussion_id), INDEX IDX_10F99812FC6CD52A (ressource_id), PRIMARY KEY(discussion_id, ressource_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE discussion_ressource ADD CONSTRAINT FK_10F998121ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discussion_ressource ADD CONSTRAINT FK_10F99812FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE discussion DROP FOREIGN KEY FK_C0B9F90FCA7FDC09');
        $this->addSql('DROP INDEX IDX_C0B9F90FCA7FDC09 ON discussion');
        $this->addSql('ALTER TABLE discussion DROP ressource_support_id');
    }
}
