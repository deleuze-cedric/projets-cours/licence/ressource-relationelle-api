<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122124045 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categorie_ressource DROP deleted_date');
        $this->addSql('ALTER TABLE ressource DROP activated_date');
        $this->addSql('ALTER TABLE type_relation DROP deleted_date');
        $this->addSql('ALTER TABLE type_ressource DROP deleted_date');
        $this->addSql('ALTER TABLE user ADD activated_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE categorie_ressource ADD deleted_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ressource ADD activated_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE type_relation ADD deleted_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE type_ressource ADD deleted_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP activated_date');
    }
}
